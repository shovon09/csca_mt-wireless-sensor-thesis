\fakechapter 
\contentsline {chapter}{Abstract}{i}
\fakechapter 
\contentsline {chapter}{Table of Contents}{ii}
\fakechapter 
\contentsline {chapter}{List of Figures}{iii}
\realchapter 
\fakechapter 
\contentsline {chapter}{List of Tables}{v}
\realchapter 
\fakechapter 
\contentsline {chapter}{List of Algorithms}{vi}
\realchapter 
\contentsline {chapter}{\numberline {1}Introduction}{1}
\contentsline {section}{\numberline {1.1}Introduction}{1}
\contentsline {section}{\numberline {1.2}Applications of Wireless Sensor Network}{2}
\contentsline {section}{\numberline {1.3}Research in Wireless Sensor Network}{3}
\contentsline {section}{\numberline {1.4}Energy Efficiency in Wireless Sensor Network}{5}
\contentsline {section}{\numberline {1.5}Description of Problem}{5}
\contentsline {section}{\numberline {1.6}Our Contribution}{6}
\contentsline {section}{\numberline {1.7}Outline of the Thesis}{6}
\contentsline {chapter}{\numberline {2}Literature Review}{8}
\contentsline {section}{\numberline {2.1}Introduction}{8}
\contentsline {section}{\numberline {2.2}Broadcast operation in Duty-Cycled Wireless Sensor Networks}{8}
\contentsline {section}{\numberline {2.3}Summary}{11}
\contentsline {chapter}{\numberline {3}Proposed Algorithm}{12}
\contentsline {section}{\numberline {3.1}Introduction}{12}
\contentsline {section}{\numberline {3.2}Problem Statement}{12}
\contentsline {section}{\numberline {3.3}Algorithm}{13}
\contentsline {subsection}{\numberline {3.3.1}Centralized Set Cover based approximation with Minimum Transmission(CSCA\_MT)}{13}
\contentsline {subsection}{\numberline {3.3.2}Brief Description of CSCA\_MT}{17}
\contentsline {subsection}{\numberline {3.3.3}Why Our Algorithm Improves the Performance}{31}
\contentsline {chapter}{\numberline {4}Experimental Results}{32}
\contentsline {section}{\numberline {4.1}Introduction}{32}
\contentsline {section}{\numberline {4.2}Performance Comparison}{32}
\contentsline {subsection}{\numberline {4.2.1}Total Number of Data Transmissions}{33}
\contentsline {subsection}{\numberline {4.2.2}Average Height of Broadcast Tree }{34}
\contentsline {subsection}{\numberline {4.2.3}Average Delay}{36}
\contentsline {section}{\numberline {4.3}Result Analysis}{37}
\contentsline {chapter}{\numberline {5}Conclusions}{38}
\contentsline {section}{\numberline {5.1}Summary of Research}{38}
\contentsline {section}{\numberline {5.2}Future Works}{38}
\fakechapter 
\contentsline {chapter}{Bibliography}{39}
\ttl@change@i {\@ne }{chapter}{6pc}{\bfseries }{\contentslabel [\appendixname \ \thecontentslabel ]{6pc}}{}{\hfill \contentspage }\relax 
\ttl@change@v {chapter}{}{}{}\relax 
\contentsfinish 
